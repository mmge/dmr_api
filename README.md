# dmr_api

API for interfacing with DMR to collect orders and distribute catalogs

## Starting a server

From the apps home directory type:

```node index.js [args]```

Where [args] can be any of the following:

|             |    | Description                         | type   | default   |
|-------------|----|-------------------------------------|--------|-----------|
| --port      | -p | Port where the server should listen | number | 3000      |
| --db_loc    | -l | Location of the mongodb instance    | string | localhost |
| --db_name   | -n | Name of the mongodb database to use | string | dmr       |
| --api_entry | -e | The entrypoint for the api          | string | /api/v1   |
| --key       | -k | location of an ssl key file         | string | undefined |
| --cert      | -c | location of an ssl cert file        | string | undefined |
| --console_level| | level of messages to display on console| string | info   |
| --default_format| | the default format to return if none specified | string | json |
| --env       |    | the environment in which to run the api | string | production | 

You can use the short or long versions of many of the arguments but the long 
versions take precedent if there is a conflict. For example:

```
$ node index.js --port=3001 -p 3002
```
will start a server listening on port 3001 because that is the port that was
declared with the long version of the argument.

If a SSL key and cert are provided then an https server will be started, 
otherwise an http server will start.

To stop a running server press Ctrl-C in the console where it is running.

#### Examples

This starts a server with default settings:
```
$ node index.js
```

This starts an https server listening on port 3001:
```
$ node index.js --port=3001 --key="key.pem" --cert="cert.pem"
```

This is the same as the example above but uses shorthand arguments:
```
$ node index.js -p 3000 -k "key.pem" -c "cert.pem"
```

This would start an api server listening on port 3001 and connect the the ppmi 
database running on a server located at 10.101.101.101:
```
$ node index.js --port=3001 --db_name="ppmi" --db_loc="10.101.101.101"
```

## Endpoints

https://www.biosend.org/api
