// minimist allows use of command line arguments with node applications.
var argv = require("minimist")(process.argv.slice(2));

// Pulls in command line arguments or assigns a default value if there
// is no command line argument.
var opts = {
	port: argv['port'] || argv['p'] || 3000,
	db_loc: argv['db-loc'] || argv['l'] || 'localhost',
	db_name: argv['db-name'] || argv['n'] || "dmr",
	api_entry: argv['api-entry'] || argv['e'] || "/api/v1",
	key: argv['key'] || argv['k'] || undefined,
	cert: argv['cert'] || argv['c'] || undefined,
	console_level: argv['console-level'] || 'info',
	default_format: argv['return_format'] || argv['f'] || 'json',
	env: argv["env"] || 'production'
};

// Build the api server with the provided options.
var apiServer = require("./lib/server.js")(opts);

// Start the server.
var server = apiServer.startServer();
