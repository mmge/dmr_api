#!/bin/bash

SECRET=$(openssl rand -base64 64)

printf "module.exports = function() {
        return '${SECRET}';
}\n"| tee lib/secret.js
