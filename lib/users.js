
module.exports = function(app) {

	logger = app.logger;

	var express = require("express");
	var passport = require('passport');
	var BasicStrategy = require('passport-http').BasicStrategy;	
	
	var route = express.Router();
	app.locals.User = app.locals.User;
	
	var basicAuth = require("./middleware/basicAuth.js")(app);
	var authorize = require("./middleware/authorize.js");

	route.use(basicAuth);

	// ADD MIDDLEWARE HERE //
		
	var libPath = "./users";

	var routes = [
		{
			method: "post",
			middleware: [authorize(['admin'])],
			route: "/",
			lib: require(libPath + "/createUser.js")()
		},
		{
			method: "delete",
			middleware: [authorize(['admin'])],
			route: "/:userID",
			lib: require(libPath + "/deleteUser.js")()
		},
		{
			method: "put",
			middleware: [],
			route: "/:userID",
			lib: require(libPath + "/updateUser.js")()
		},
		{
			method: "get",
			middleware: [authorize(['admin'])],
			route: "/:userID.:format",
			lib: require(libPath + "/getUser.js")()
		}
	];
	
	for(var i = 0; i < routes.length; i++) {
		r = routes[i];
		logger.debug("Building " + r.method + " method for " + r.route);
		route[r.method](r.route, r.middleware, r.lib);
		logger.silly("Built " + r.method + " method for " + r.route);
	}	
	
	logger.debug("User Endpoints Created");

	return route;
		
};
