module.exports = function(app) {

	logger = app.logger;

	var express = require("express"),
		path = require("path"),
		multer = require("multer"),
		upload = multer({dest: __dirname + "/../uploads"}),
		orderUpload = upload.fields([
			{name: 'catalog', maxCount: 1},
			{name: 'attachments', maxCount: 3}
		]);
		
	var route = express.Router();
	var jwtAuth = require("./middleware/jwtAuth.js")(app);
	var authorize = require("./middleware/authorizeToken.js");
	// ADD MIDDLEWARE HERE //
	
	route.use(jwtAuth);
	var libPath = "./catalogs";
	
	var routes = [
		{
			method: "get",
			middleware: [authorize("all_roles", "view")],
			route: "/:catalog.:format",
			lib: require(libPath + "/getCatalog.js")()
		},
		{
			method: "post",
			middleware: [authorize(["admin", "internal"], "create"), orderUpload],
			route: "/",
			lib: require(libPath + "/postCatalog.js")()
		}
	];
	
	for(var i = 0; i < routes.length; i++) {
		r = routes[i];
		logger.debug("Building " + r.method + " method for " + r.route);
		route[r.method](r.route, r.middleware, r.lib);
		logger.silly("Built " + r.method + " method for " + r.route);
	}	

	logger.debug("Catalog Endpoints Created");

	return route;
	
};
