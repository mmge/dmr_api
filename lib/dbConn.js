// Responsible for setting up the connection to the mongodb server and setting up gridfs
module.exports = function(app, options, callback) {
	
	var mongoose = require("mongoose"),
		grid = require("gridfs-stream");
	
	if(options === undefined) options = {db_loc: "localhost", db_name: "dmr"};

	mongoose.Promise = Promise;
	mongoose.connect('mongodb://' + options.db_loc + '/' + options.db_name);
	app.logger.debug("Connected to mongodb...");

	var conn = mongoose.connection;

	mongoose.connection.on('error', console.error.bind(console, 'connection error:'));

	grid.mongo = mongoose.mongo;
	
	app.database = mongoose;
	app.grid = grid;	
		
	// add all the mongoose models to the app
	app.locals.Order = require("./orders/orderSchema.js")(app);
	app.locals.User = require("./users/userSchema.js")(app);
	app.locals.Token = require("./tokens/tokenSchema.js")(app);
	app.locals.Catalog = require("./catalogs/catalogSchema.js")(app);
	app.locals.Quote = require("./quotes/quoteSchema.js")(app);
		
	conn.once('open', function() {
		app.logger.info("Connected to mongodb database " + options.db_name + " at " + options.db_loc);
		callback();
	});	
	
};
