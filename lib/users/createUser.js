module.exports = function() {
	
	return function(req, res, next) {
		
		var logger = req.app.logger;
		
		logger.debug(req.user.role);
		
		if(req.body.username && req.body.password) {
		
			var user = new req.app.locals.User({
				username: req.body.username,
				password: req.body.password
			});

			if(req.body.role) {
				user.role = req.body.role;
			}
			
			user.save(function(err) {
				
				if(err) {
					err.status = 400;
					return next(err);
				}
				
				res.locals.status = 201;
				res.locals.message = "User " + user.username + " created with " + user.role + " privileges.";
				return res.respond();
				
			});
			
		} else {
			var err = new Error();
			err.status = 400;
			err.message = "Missing username and/or password in body of POST.";
			return next(err);
		};
		
	};
	
};
