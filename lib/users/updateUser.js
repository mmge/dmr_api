module.exports = function() {
	
	return function(req, res, next) {
		
		var logger = req.app.logger;
		
		if(req.user.role != "admin") {
			
			if(req.user.username != req.params.userID) {
				var err = new Error();
				err.status = 403;
				err.message = "You may only update your own password.";
				return next(err);
			} else {
				req.app.locals.User.findOne({username: req.params.userID}, function(err, doc) {
					
					if(err) {
						err.status = 500;
						err.message = "Error locating user in database.";
						return next(err);						
					}
					
					if(req.body.role) {
						var err = new Error();
						err.status = 403;
						err.message = "You are not authorized to update your role.";
						return next(err);
					}
					
					if(req.body.password) {
						doc.password = req.body.password;
						doc.save(function(err) {
							if(err) {
								err.status = 500;
								err.message = "Could not update user information in database.";
								return next(err);
							}
							
							res.locals.status = 200;
							res.locals.message = "You have successfully updated your master password.";
							res.respond();
												
						});					
					} else {
						var err = new Error();
						err.status = 400;
						err.message = "You must provide a new password in the body of the request.";
						return next(err);
					}
					
				});
			}
				
		} else {
			
			req.app.locals.User.findOne({username: req.params.userID}, function(err, doc) {
				
				if(err) {
					err.status = 500;
					err.message = "Error locating user in database.";
					return next(err);						
				}				
				
				if(req.body.password) {
					doc.password = req.body.password;
				}
				
				if(req.body.role) {
					doc.role = req.body.role;
				}
				
				doc.save(function(err) {
					if(err) {
						err.status = 500;
						err.message = "Could not update user information in database.";
						return next(err);
					}
					
					res.locals.status = 200;
					res.locals.message = "User " + req.params.userID + " was updated.";
					res.respond();
										
				});
				
			});
				
		}
	};
	
};
