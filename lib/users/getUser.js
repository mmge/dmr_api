module.exports = function() {


	return function(req, res, next) {
		
		var logger = req.app.logger;

		req.app.locals.User.findOne({username: req.params.userID}, function(err, doc) {
			
			if(err) {
				err.status = 500;
				err.message = "Error locating user in database."
				return next(err);						
			}
			
			res.locals.status = 200;
//			res.locals.message = doc;
			res.locals.message = {username: doc.username, 
				role: doc.role, 
				locked: doc.locked,
				lastLogin: doc.lastLogin}; 
			return res.respond();
			
		});		
		
	};

};
