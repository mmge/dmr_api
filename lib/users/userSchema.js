module.exports = function(app) {
	
	var fs = require("fs"),
		bcrypt = require("bcryptjs"),
		SALT_WORK_FACTOR = 10, // The level of password encryption. higher == stronger but slower.
		maxAttempts = 5; // The number of consecutive bad passwords before locking the account.
	
	var logger = app.logger,
		db = app.database;
		
	var userSchema = new db.Schema({
		username: {type: String, required: true, index: true, unique: true},
		password: {type: String, required: true},
		lastLogin: {type: Date},
		lastLoginAttempt: {type: Date},
		locked: {type: Boolean, default: false},
		badTries: {type: Number, default: 0},
		role: {
			type: String, 
			enum: ["admin", "internal", "external", "viewer"],
			default: "viewer"
		}			
	});
	
	// This encrypts the password before any user save to the database.
	userSchema.pre('save', function(next) { 
		
		var user = this;
		
		if(!user.isModified('password')) return next();
		
		//generate salt
		bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
			
			if(err) return next(err);
			
			//hash the password along with our new salt
			bcrypt.hash(user.password, salt, function(err, hash) {
				
				if(err) return next(err);
				
				//override the cleartext password with the hashed one
				user.password = hash;
				next();
				
			});
		});
		
	});
	
	userSchema.methods.login = function(candidatePassword, cb) {
		
		// Store user instance
		var thisUser = this;
		
		// Updates last login attempt
		this.lastLoginAttempt = Date.now();
		
		
		// Checks to make sure the account hasn't been locked
		if(this.locked) {
			var lockedError = new Error();
			lockedError.status = 403;
			lockedError.message = "This account has been locked. Please contact the system administrator for assistance.";
			return cb(lockedError);
		}
		
		bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
			
			if(err) {
				// If login fails, increment the badTries counter
				thisUser.badTries++;
				// If badTries counter gets too high, lock the account.
				if(thisUser.badTries >= maxAttempts) thisUser.locked = true;
				// Save the updated error data to the database.
				thisUser.save(function(err2) {
					if(err2) return cb(err2);
					err.status = 403;
					return cb(err);
				});
			}
								
			if(isMatch) {
				// If passwords checks out make sure the badTries counter is 
				// reset and saved before returning the success				this.User.badTries = 0;
				thisUser.lastLogin = Date.now();
			} else {
				// If it doesn't match increment the counter and if the counter
				// exceeds the allowable bad tries, lock out the user
				thisUser.badTries++;
				if(thisUser.badTries >= maxAttempts) thisUser.locked = true;

			}

			thisUser.save(function(err) {
				
				if(err) return cb(err);	
				cb(null, isMatch);
				
			});
			
		});
		
	};
	
	User = db.model('User', userSchema);
	
	return User;
	
};
