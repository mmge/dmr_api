module.exports = function() {

	return function(req, res, next) {

		var logger = req.app.logger;

		req.app.locals.User.find({username: req.params.userID}).remove(function(err, v) {

			if(err) {
				err.status = 500;
				err.message = "The user could not be removed at this time";
				return next(err);
			}
			if(v.result.n > 0) {
				res.locals.status = 200;
				res.locals.message = "User " + req.params.userID + " removed from system.";
				
				req.app.locals.Token.update({issuedBy: req.params.userID}, {$set: {valid: false}}, function(err) {
					if(err) {
						res.locals.status = 500;
						res.locals.message += " There was an error when attempting to revoke tokens issued by user. Please revoke tokens manually.";
					} else {
						res.locals.message += " All tokens issued by user have been revoked.";
					}
					return res.respond();
				});
				
			} else {
				var err = new Error();
				err.status = 400;
				err.message = "User " + req.params.userID + " could not be found.";
				return next(err);
			}

		});

	};

};
