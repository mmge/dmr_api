module.exports = function(app) {
	
	var logger = app.logger;
	
	var express = require("express"),
		path = require("path"),
		multer = require("multer"),
		upload = multer({dest: __dirname + "/../uploads"}),
//		upload = multer({storage: multer.memoryStorage()}),
		orderUpload = upload.fields([
			{name: 'order', maxCount: 1},
			{name: 'attachments', maxCount: 10}
		]);	
		
	var route = express.Router();
	var readOrder = require("./middleware/readOrder.js")();
	var fillOrder = require("./middleware/fillOrder.js")();
	var processOrder = require("./middleware/processOrder.js")();
	var jwtAuth = require("./middleware/jwtAuth.js")(app);
	var authorize = require("./middleware/authorizeToken.js");
	
	route.use(require("./middleware/validatePath.js")("orders"));
	route.use(jwtAuth);
	
	logger.debug("Building order endpoints");
	
	var libPath = "./orders";
		
	var routes = [
		{
			method: "get",
			middleware: [authorize("all_roles", "view"), fillOrder], 	
			route: "/:orderID/order.:format", 		
			lib: require(libPath + "/getOrderDetail.js")("order")
		},
		{
			method: "get",  	
			middleware: [authorize("all_roles", "view"), fillOrder], 	
			route: "/:orderID/manifest.:format", 	
			lib: require(libPath + "/getOrderDetail.js")("manifest")
		},
		{
			method: "get",		
			middleware: [authorize("all_roles", "view"), fillOrder], 	
			route: "/:orderID/status.:format", 		
			lib: require(libPath + "/getOrderDetail.js")("status")
		},
		{
			method: "get", 	
			middleware: [authorize("all_roles", "view"), fillOrder], 	
			route: "/:orderID/docs.:format",		
			lib: require(libPath + "/getOrderDetail.js")("attachments")
		},
		{
			method: "get",		
			middleware: [authorize("all_roles", "view"), fillOrder], 				
			route: "/:orderID/docs/:docID",			
			lib: require(libPath + "/getDoc.js")()
		},
		{
			method: "get",
			middleware: [authorize(["admin", "internal", "external"], "view")],
			route: "/",
			lib: require(libPath + "/getOrders.js")()
		},
		{
			method: "put",		
			middleware: [authorize(['admin', 'internal', "external"], "modify"), orderUpload, readOrder, processOrder, fillOrder], 	
			route: "/:orderID",						
			lib: require(libPath + "/putOrder.js")()
		},
		{
			method: "post",	
			middleware: [authorize(["admin", "external"], "create"), orderUpload, readOrder], 
			route: "/",
			lib: require(libPath + "/postOrder.js")()
		}
	];
	
	for(var i = 0; i < routes.length; i++) {
		r = routes[i];
		logger.debug("Building " + r.method + " method for " + r.route);
		route[r.method](r.route, r.middleware, r.lib);
		logger.silly("Built " + r.method + " method for " + r.route);
	}	

	// View the order form
	logger.debug("Building get method for /");
	route.get("/", function(req, res) {
		logger.debug("Order Form Requested...");
		res.sendFile(path.join(__dirname, "../static/order_form.html"));
	});
	logger.silly("Built get method for /");
						
	logger.debug("Order Endpoints Created");

	return route;
	
};
