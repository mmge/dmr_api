module.exports = function(app) {

	var logger = app.logger;
	
	var express = require("express"),
		path = require("path"),
		bodyParser = require("body-parser");
		
	var route = express.Router();
	var jwtAuth = require("./middleware/jwtAuth.js")(app);
	var authorize = require("./middleware/authorizeToken.js");
	
	route.use(jwtAuth);
	
	logger.debug("Building test endpoints");
	
	var libPath = "./test";
	
	var routes = [
		{
			method: "get",
			middleware: [authorize("admin", "view")],
			route: "/",
			lib: require(libPath + "/getTest.js")()
		}
	];
	
	for(var i = 0; i < routes.length; i++) {
		r = routes[i];
		logger.debug("Building " + r.method + " method for " + r.route);
		route[r.method](r.route, r.middleware, r.lib);
		logger.silly("Built " + r.method + " method for " + r.route);
	}	

	logger.debug("Test Endpoints Created");

	return route;
	

};
