
module.exports = function() {
		
	return function(req, res, next) {
		
		var logger = req.app.logger;
		
		res.locals.quote = new res.app.locals.Quote();
		
		res.locals.quote.add(req.body, function(err) {
			if(err) {
				return next(err);
			}
			
			logger.debug("Processing Quote...");
			
			res.locals.quote.save(function(err) {
				if(err) return next(err);
				res.locals.status = 201;
				res.locals.message = "Quote was created.";
				return res.respond();		
			});
			
		});
		
	};
	
};
