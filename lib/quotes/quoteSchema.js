module.exports = function(app) {

	var fs = require("fs");

	var logger = app.logger,
		db = app.database,
		grid = app.grid;

	var quoteSchema = new db.Schema({
		firstName: String,
		lastName: String,
		institution: String,
		email: String,
		phone: String,
		foa: Boolean,
		foaNumber: String,
		contactedNINDS: Boolean,
		NINDScontact: String,
		activeEnrollment: Boolean,
		subjectCount: String,
		startDate: String,
		studyLength: String,
		sampleTypes: [String],
		notes: String,
		reviewerNotes: String,
		status: {type: String, default: "received"},
		dateReceived: {type: Date, default: Date.now},
		viewed: {type: Boolean, default: false}
	});
	
	quoteSchema.methods.add = function(body, cb) {
		
		var quoteInst = this;
		logger.debug("Processing Quote Request...");
		
		logger.debug(Object.keys(body));
		
		quoteInst.set({
			firstName: body.First_Name || "",
			lastName: body.Last_Name || "",
			institution: body.Company || "",
			email: body.Email || "",
			phone: body.Phone || "",
			foa: body.FOA_Response == "Yes" ? true : false || false,
			foaNumber: body.FOA_Number || "",
			contactNINDS: body.Officer_Contact === "Yes" ? true : false || false,
			NINDScontact: body.Officer_Name || "",
			activeEnrollment: body.Active_Study === "Yes" ? true : false || false,
			subjectCount: body.Number_Enrolled || "",
			startDate: body.Start_Date || "",
			studyLength: body.Study_Length || "",
			sampleTypes: body.Sample_Types.split("|") || [],
			notes: body.Additional_Comments || ""
		});
		
		quoteInst.save();
		
		return cb();
		
	};
	
	
	
	var Quote = db.model('Quote', quoteSchema);
			
	return Quote;
	
};
				
	
