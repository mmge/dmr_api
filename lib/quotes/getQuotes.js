
module.exports = function() {
		
	return function(req, res, next) {
		
		var logger = req.app.logger;
		var query = req.query;
		
		var keys = Object.keys(query);
		
		for(var i = 0; i < keys.length; i++) {
			query[keys[i]] = query[keys[i]].split(",");
		}
		
		var q = {};
		
		if(keys.indexOf("lastName") !== -1) {
			q.lastName = {$in: query.lastName};
		}

		if(keys.indexOf("institution") !== -1) {
			q.institution = {$in: query.institution};
		}
		
		var quotes = req.app.locals.Quote.find(q,
			function(err, records) {
				if(err) return next(err);
				res.respond(records);
			}
		);
				
	};
	
};
