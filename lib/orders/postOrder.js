module.exports = function() {
	
	var fs = require("fs"),
		xml2js = require("xml2js"),
		is = require("type-is");	
		
	return function(req, res, next) {
		
		var logger = req.app.logger;
		
		if(res.locals.order === undefined) {
			var noErr = new Error();
			noErr.status = 400;
			noErr.message = "No order file was uploaded with request.";
			return next(noErr);
		}
		
		res.app.locals.Order.checkExistence(res.locals.order.id, function(err, exists) {
			
			if(err) {
				
				err.status = 500;
				err.message = "Problems searching the database for existing orders";
				logger.error(err);
				return next(err);
				
			} 
			
			if(exists) {
				
				var err = new Error();
				err.status = 400;
				err.message = "Order " + res.locals.order.id + " already exists. Please PUT an update to this order or POST it with a new order id.";
				logger.error(err);
				return next(err);			
				
			} else {
				
				res.locals.order.addAttachments(req.files.attachments, function(err, val) {
					
					if(err) {
						return next(err);
					}
					
					res.locals.order.save(function(err, val) {
						if(err) return next(err);
						res.locals.status = 201;
						res.locals.message = "Order " + res.locals.order.id + " was created.";
				
						res.respond();
						
						logger.debug("Order " + res.locals.order.id + " was accepted.");
						
					});
				
				});				
				
			}
									
		});		
		
		
	};	
	
};
