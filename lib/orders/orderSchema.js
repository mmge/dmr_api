module.exports = function(app) {

	var fs = require("fs");

	var logger = app.logger,
		db = app.database,
		grid = app.grid;

	var orderSchema = new db.Schema({
		id: {type: String, required: true, index: true},
		oncoreId: {type: String, required: false, index: true, default: ""},
		dateReceived: {type: Date, required: true, default: Date.now},
		dateLastUpdated: {type: Date, required: true, default: Date.now},
		status: {type: String, default: "received"},
		uploadedBy: {type: String},
		order: {
			originator: {
				name: {type: String, required: true},
				email: {type: String, required: true}
			},
			date: {type: Date, required: true},
			user: {
				firstName: {type: String, required: true},
				lastName: {type: String, required: true},
				email: {type: String, required: true},
				phone: {type: String, required: true}
			},
			institution: {
				name: {type: String, required: true}
			},
			grant: {
				abstract: {type: String}
			},
			powerAnalysis: String,
			shipTo: {
				name: {type: String, required: true},
				organization: {type: String, required: true},
				phone: {type: String, required: true},
				address: {type: String, required: true},
				address2: String,
				city: {type: String, required: true},
				zip: {type: String, required: true},
				affiliation: {
					name: {type: String, required: true},
					phone: {type: String, required: true},
					email: {type: String, required: true},
					specialInstructions: String
				}
			},
			samples: [{
				sampleId: {type: String, required: true},
				guid: {type: String, required: true},
				sampleType: {type: String, required: true},
				originalContainer: String,
				visit: {type: String, required: true},
				number: {type: Number, required: true},
				repository: {type: String, required: true},
				_id: {type: "ObjectId", select: false}
			}]
		},
		manifest: [{
			st_number: String,
			guid: String,
			barcode: String,
			specimen_type: String,
			specimen_quantity: Number,
			specimen_uom: String,
			concentration: Number,
			concentration_uom: String,
			rin: Number,
			x260_280: Number,
			x260_230: Number,
			hemoglobin_assay: Number,
			turbidity: String,
			_id: {type: "ObjectId", select: false}
		}],
		attachments: [String]
	});

	orderSchema.statics.clearManifest = function() {
		
		this.manifest = [];
		
	};

	orderSchema.statics.getByQuery = function(query, user, cb) {
		
		var q = {};
			
		var keys = Object.keys(query);
		
		// add status to query
		if(keys.indexOf("status") !== -1) {
			q.status = {$in: query.status};
		}

		// query based on earliest date
		if(keys.indexOf("beginDate") !== -1) {
			q.dateLastUpdated = {$gte: new Date(query.beginDate)};
		}

		// query based on latest date
		if(keys.indexOf("endDate") !== -1) {
			if(Object.keys(q).indexOf("dateLastUpdate") === -1) {
				q.dateLasteUpdated = {$lte: new Date(query.endDate)};
			} else {
				q.dateLasteUpdated.$lte = new Date(query.endDate);
			}
		}
		
		if(['admin', 'internal'].indexOf(user.scope.role) > -1) {
			this.find(q).select('-_id id oncoreId status order').exec(function(err, orders) {
				
				if(err) return cb(err);
				
				cb(null, {count: orders.length, orders: orders});
				
			});
		} else {
			this.find(q).select('-_id id status order').exec(function(err, orders) {
				
				if(err) return cb(err);
				
				cb(null, {count: orders.length, orders: orders});
				
			});
		}

	};

	orderSchema.methods.addAttachments = function(attachments, cb) {
		
		var att = [],
			gfs = grid(db.connection.db),
			writestream;
		
		if(attachments) {
			var j;
			for(i = 0; i < attachments.length; i++) {
				writestream = gfs.createWriteStream({
					filename: attachments[i].originalname,
					metadata: {
						orderId: this.id,
						documentId: this._id,
						originalname: attachments[i].originalname,
						encoding: attachments[i].encoding,
						mimetype: attachments[i].mimetype,
						size: attachments[i].size
					}
				});
				
				att.push(attachments[i].originalname);
				fs.createReadStream(attachments[i].path).pipe(writestream);
				fs.unlink(__dirname + "/../../uploads/" + attachments[i].filename);
			}
			
			this.set({attachments: att});	
			
		}
		
		cb(null, this);	
		
	};
	
	orderSchema.statics.checkExistence = function(id, cb) {
		
		logger.debug("Checking for existence of order " + id);
		
		Order.find({id: String(id), status: {$nin: ['canceled', 'changed']}}, function(err, order) {
			
			logger.debug("Looking for a matching existing order");
			
			if(err) {
				return cb(err, null);
			}
			
			if(order.length === 0) {
				return cb(null, false);
			} else {
				return cb(null, order.length);
			}
			
		});
		
	};
	
	orderSchema.methods.update = function(update, cb) {
		
		update.dateLast = Date.now();
		
		Order.update({_id: this._id}, update, cb);
		
	};
	
	orderSchema.methods.updateStatus = function(status, cb) {
		
		Order.update({_id: this._id}, {status: status, dateLastUpdated: Date.now()}, cb);
		
	};
	
	orderSchema.methods.addOncoreId = function(oncoreId, cb) {
		
		Order.update({_id: this._id}, {oncoreId: oncoreId, dateLastUpdated: Date.now()}, cb);
		
	};
	
	orderSchema.methods.getById = function(id, cb) {
		
		var orderInst = this;
		
		Order.checkExistence(id, function(err, exists) {
			
			if(err) {
				return cb(err);
			}
			
			var err = new Error();
			
			if(exists > 1) {
				err.status = 500;
				err.message = "Multiple orders matched this request. Please contact the administrator.";
				return cb(err);
			} else if(!exists) {
				err.status = 404;
				err.message = "The requested order could not be found.";
				return cb(err);				
			}
			
			Order.find({id: String(id), status: {$nin: ['canceled', 'changed']}}, function(err, order) {
			
				if(err) return cb(err);
			
				console.log(order[0]);
			
				orderInst.set(order[0]);
			
				cb();
				
			});
				
		});
		
	};
	
	orderSchema.methods.getFromBody = function(body, cb) {
		
		logger.debug("Processing order from body...");
			
		var orderInst = this;
		logger.debug("BODY:\n" + body + "\n-----");
		
		try{
			var order = JSON.parse(body);
		
			logger.debug(order);
		} catch(e) {
			throw new Error("Body not read");
		}
		
		orderInst.set({
			id: order.id,
			order: {
				originator: order.originator,
				date: order.date,
				user: order.user,
				institution: order.institution,
				grant: order.grant,
				powerAnalysis: order.powerAnalysis,
				shipTo: order.shipTo,
				samples: order.samples		
			}
		});
		
		return cb();
		
	};
	
	orderSchema.methods.getFromJSON = function(json, cb) {
		
		var orderInst = this;
		logger.debug("Processing Order JSON...");
		
		orderInst.set({
			id: json.id,
			order:{
				originator: json.originator,
				date: "",
				user: json.user,
				institution: json.institution,
				grant: json.grant,
				powerAnalysis: json.powerAnalysis,
				shipTo: json.shipTo,
				samples: samples
			}
		});	
		
		return cb();
				
	};
	
	orderSchema.methods.getFromDMRXML = function(xml, cb) {
		
		var orderInst = this;
		logger.debug("Processing DMR XML...");
		
		var sample, samples = [], 
			rawSamples = xml.SampleRequests.SampleRequest;
			
		// If only one sample is submitted it won't be an array and won't
		// get picked up, so anything that isn't an array gets turned into
		// one.
		if(!Array.isArray(rawSamples)) {
			rawSamples = [rawSamples];
		}		
			
		for(var i = 0; i < rawSamples.length; i++) {
			sample = rawSamples[i];
			logger.silly("Processing Sample Request " + i);
			
			samples.push({
				
				sampleId: sample.SampleId || sample.CoriellId,
				guid: sample.GUID,
				sampleType: sample.SpecimenType,
				originalContainer: sample.OriginalContainerTypeReceived,
				visit: sample.VisitDescription,
				number: sample.NumberOfAliquots,
				repository: sample.Repository
				
			});
					
		}

		logger.debug("Setting data in the order model...");
		orderInst.set({
			id: xml.OrderMetadata.OriginatorOrderId,
			order:{
				originator:{
					name: xml.OrderMetadata.Originator,
					email: xml.OrderMetadata.OriginatorEmail
				},
				date: xml.Date,
				user: {
					firstName: xml.User.FirstName,
					lastName: xml.User.LastName,
					email: xml.User.Email,
					phone: xml.User.Phone
				},
				institution: {
					name: xml.Institution.Name
				},
				grant: {
					abstract: xml.Grant.Abstract
				},
				powerAnalysis: xml.PowerAnalysis,
				shipTo: {
					name: xml.ShippingAddress.ShipToName,
					organization: xml.ShippingAddress.ShipToOrganization,
					phone: xml.ShippingAddress.ShipToPhone,
					address: xml.ShippingAddress.ShipToAddress,
					address2: xml.ShippingAddress.ShipToAddress2,
					city: xml.ShippingAddress.ShipToCity,
					state: xml.ShippingAddress.ShipToState,
					zip: xml.ShippingAddress.ShipToZip,
					affiliation: {
						name: xml.ShippingAddress.ShipToAffiliation,
						phone: xml.ShippingAddress.ShipToAffiliationPhone,
						email: xml.ShippingAddress.ShipToAffiliationEmail,
						specialInstructions: xml.ShippingAddress.ShipToAffiliationSpecialInstructions
					}
				},
				samples: samples
			}
		});	
		
		return cb();			
				
	};
	
	Order = db.model('Order', orderSchema);
			
	return Order;
	
};
				
	
