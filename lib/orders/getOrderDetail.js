module.exports = function(section) {

	return function(req, res, next) {

		res.locals.status = 200;
		
		var msg = res.locals.order[section];

		if(section === "manifest") {
			msg = {sample: msg};
		}
		
		if(section == "order") {
			msg.orderId = res.locals.order.id;
			
			if(['admin', 'internal'].indexOf(req.user.scope.role) > -1) {
				msg.oncoreId = res.locals.order.oncoreId;
			}
		}
		
		res.locals.message = msg;
	
		res.respond();
	
	};

};
