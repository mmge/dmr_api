model.exports = function(app) {

	var logger = app.logger,
	db = app.database,
	grid = app.grid;
	
	var options = {discriminatorKey: "kind"};

	var manifestSchema = new db.Schema({
		order_id: String,
		st_number: String,
		barcode: String,
		specimen_type: String,
		specimen_quantity: Number,
		specimen_uom: String,
		concentration: Number,
		rin: Number,
		x260_280: Number,
		hemoglobin_assay: Number,
		turbidity: String
	});
	
	var Manifest = db.model('Manifest', manifestSchema, options);
	
	var RNAManifest = Manifest.discriminator("RNAManifest",
		new db.Schema({
			concentration: Number,
			rin: Number,
			x260_280: Number
		}, options));
		
	var DNAManifest = Manifest.discriminator("DNAManifest",
		new dbSchema({
			concentration: Number
		}, options));
	
	var OtherManifest = Manifest.discriminator("OtherManifest", 
		new dbSchema({
			hemoglobin_assay: Number,
			turbidity: String
		}, options));

	Manifest.statics.create = function(man, callback) {
		
		switch(man.specimen_type) {
			
			case "RNA":
			
				var manifest = new RNAManifest(man);
				break;
				
			case "DNA":
				
				var manifest = new DNAManifest(man);
				break;
				
			default:
				
				var manifest = new OtherManifest(man);
				break;
				
		}
		
		manifest.save(function(err) {
			
			if(err) return callback(err);
			return callback();
			
		});
		
	};

};
