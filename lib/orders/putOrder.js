module.exports = function(section) {

	return function(req, res, next) {

		var logger = req.app.logger;
		var type = req.body.updateType;
		var keys = Object.keys(req.body);
		
		if(type === "cancel") {
						
			if(res.locals.order.status === "received") {
				
				res.locals.order.update({status: "canceled"}, function(err, num) {
					
					if(err) return next(err);
					
					if(num.nModified === 0) {
						
						var err = new Error();
						err.status = 500;
						err.message = "There was an error when cancelling your order.";
						return next(err);
						
					} else {
												
						res.locals.status = 200;
						res.locals.message = "Cancellation request for order " + res.locals.order.id + " accepted.";
						return res.respond();
						
					}
				});
				
			} else if(res.locals.order.status === "pending") {
				
				
				if(['admin', 'internal'].indexOf(req.user.scope.role) > -1) {
					
					res.locals.order.update({status: "canceled"}, function(err, num) {
						
						if(err) return next(err);
						
						if(num.nModified === 0) {
							
							var err = new Error();
							err.status = 500;
							err.message = "There was an error when canceling the order.";
							return next(err);
							
						} else {
													
							res.locals.status = 200;
							res.locals.message = "Cancellation request for order " + res.locals.order.id + " accepted.";
							return res.respond();
							
						}
						
					});
					
				} else {
				
					res.locals.order.update({status: "cancellation requested"}, function(err, num) {
						
						if(err) return next(err);
						
						if(num.nModified === 0) {
							
							var err = new Error();
							err.status = 500;
							err.message = "There was an error when canceling your order.";
							return next(err);
							
						} else {
													
							res.locals.status = 202;
							res.locals.message = "Cancellation request for order " + 
								res.locals.order.id + " acknowledged. Your order will be canceled if possible.";
							return res.respond();
							
						}
					});
					
				}
				
			} else if(["complete", "shipped"].indexOf(res.locals.order.status) > -1) {
				
				var err = new Error();
				err.status = 403;
				err.message = "Order " + res.locals.order.id + " has already been processed and cannot be cancelled. Please contact biosend@iu.edu if you have questions.";
				return next(err);
				
			} else if(res.locals.order.status === "cancellation requested") {
				var err = new Error();
				err.status = 403;
				err.message = "A cancellation request for Order " + res.locals.order.id + " has already been received. Please wait 24 hours and then check the status of the order.";
				return next(err);
			} else {
				var err = new Error();
				err.status = 500;
				err.message = "An unknown internal server error occured. It appears that a order status that isn't allowed crept in somehow...";
				return next(err);
			}
			
		} else if(type === "update") {
			
			if(res.locals.new_order) {
				
				if(res.locals.new_order.id !== res.locals.order.id) {
					var err = new Error();
					err.status = 400;
					err.message = "The uploaded order's order id must match order id of the order being updated.";
					return next(err);
				}
				
				switch(res.locals.order.status) {
					
					case "received":

						res.locals.new_order.save(function(err) {
							
							if(err) return next(err);
							
							res.locals.order.update({status: "changed"}, function(err, num) {
								
								if(err) return next(err);
								
								if(num.nModified > 0) {
								
									res.locals.status = 202;
									res.locals.message = "Update to Order " + res.locals.order.id + " was accepted.";
									return res.respond();
										
								} else {
									
									var err = new Error();
									err.status = 500;
									err.message("Unknown Internal Server Error. More than one order was updated.");
									
								}
								
							});
								
						});
						break;
						
					case "pending":
					
						res.locals.new_order.save(function(err) {
							
							if(err) return next(err);
							
							res.locals.order.update({status: "update requested"}, function(err, num) {
								
								if(err) return next(err);
								
								if(num.nModified > 0) {
								
									res.locals.status = 202;
									res.locals.message = "Update to Order " + res.locals.order.id + " acknowledged. The order will be updated if possible.";
									return res.respond();
										
								} else {
									
									var err = new Error();
									err.status = 500;
									err.message = "Unknown Internal Server Error. More than one order was updated.";
									
								}
								
							});
								
						});
						break;					
					
					case "complete":
					case "shipped":
					
						res.locals.status = 403;
						res.locals.message = "Order " + res.locals.order.id + " cannot be updated at this time. It is too far along in processing. Please contact biosend@iu.edu if you have questions.";
						return res.respond();
						
					case "cancellation requested":
					
						res.locals.status = 403;
						res.locals.message = "Order " + res.locals.order.id + " has a pending cancellation and cannot be updated at this time.";
						return res.respond();
						
					default:
					
						var err = new Error();
						err.status = 500;
						err.message = "Unknown server error. unknown order status";
						return next(err);
				}
				
			} else if(keys.length > 0) {
								
				var up = {};
				
				if(keys.indexOf("status") !== -1) {
				
					var validStatuses = ["received", "pending", "canceled", 
						"cancellation requested", "shipped", "completed"];
				
					if(validStatuses.indexOf(req.body.status) !== -1) {
					
						up.status = req.body.status;
					
					} else {
						
						var statusError = new Error();
						statusError.status = 400;
						statusError.message = "Invalid Status: " + req.body.status;
						next(statusError);
						
					}
					
				}
				
				if(keys.indexOf("oncoreId") !== -1) {
					
					up.oncoreId = req.body.oncoreId;
					
				}
				
				if(keys.indexOf("manifest") !== -1) {
					
					up.manifest = JSON.parse(req.body.manifest);
					
				}
				
				res.locals.order.update(up, function(err, num) {
							
					if(err) return next(err);
							
					if(num.nModified === 0) {
								
						var err = new Error();
						err.status = 500;
						err.message = "There was an error when updating the order.";
						return next(err);
						
					} else {
												
						res.locals.status = 200;
						res.locals.message = "Order " + res.locals.order.id + " updated successfully'.";
						return res.respond();
						
					}
				});
						
			} else {
				
				var err = new Error();
				err.status = 400;
				err.message = "The API didn't understand the requested update.";
				return next(err);				
				
			}
			
		} else {
			
			var err = new Error();
			err.status = 400;
			err.message = "The body of a PUT request to this endpoint must contain an 'updateType' field containing either 'cancel' or 'update'.";
			return next(err);
			
		}			
			
	};

};
	

