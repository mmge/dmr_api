module.exports = function() {

	return function(req, res, next) {

		var logger = req.app.logger;
		var query = req.query;

		var keys = Object.keys(query);

		for(var i = 0; i < keys.length; i++) {
			query[keys[i]] = query[keys[i]].split(",");
		}
		
		

		req.app.locals.Order.getByQuery(query, req.user, function(err, orders) {
			
			if(err) return next(err);
			
			res.locals.status = 200;
			res.locals.message = orders;
			return res.respond();
			
		});

	};

};
