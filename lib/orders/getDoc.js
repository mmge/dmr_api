module.exports = function() {

	return function(req, res, next) {
	
		var logger = req.app.logger;
		var err = new Error();
		var grid = req.app.grid;
		var db = req.app.database;
		
		logger.debug("Retrieving Document: " + req.params.docID);
		
		if(res.locals.order.attachments.indexOf(req.params.docID) == -1) {
			err.status = 404;
			err.message = "Order '" + req.params.orderID + "' does not have an attachment with the name '" + req.params.docID +"'.";
			return next(err);
		}		
		
		var docId = req.params.docID,
			gfs = grid(db.connection.db),
			orderId = req.params.orderID;
			
		gfs.files.find({filename: docId, "metadata.documentId": res.locals.order._id}).toArray()
		.then(function(doc) {
			logger.debug(doc[0].filename);
			doc = doc[0];
			var mime = doc.metadata.mimetype
			res.set('Content-Type', mime);
			var readstream = gfs.createReadStream({_id: doc._id});
			readstream.pipe(res);			
		})
		.catch(function(err) {
			err.status = 500;
			err.message("Error retrieving document")
			return next(err);
		});
	
	};

};
