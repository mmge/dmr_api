module.exports = function(app) {

	var logger = app.logger;
	
	var express = require("express"),
		path = require("path"),
		bodyParser = require("body-parser");
		
	var route = express.Router();
	var jwtAuth = require("./middleware/jwtAuth.js")(app);
	var authorize = require("./middleware/authorizeToken.js");
	
	route.use(jwtAuth);
	
	logger.debug("Building quote endpoints");
	
	var libPath = "./quotes";
	
	var routes = [
		{
			method: "post",
			middleware: [authorize("admin", "create"), bodyParser.urlencoded({extended: false})],
			route: "/",
			lib: require(libPath + "/postQuote.js")()
		},
		{
			method: "put",
			middleware: [authorize("admin", "modify"), bodyParser.urlencoded({extended: false})],
			route: "/:quoteID",
			lib: require(libPath + "/putQuote.js")()
		},
		{
			method: "get",
			middleware: [authorize("admin", "view")],
			route: "/:quoteID",
			lib: require(libPath + "/getQuote.js")()
		},
		{
			method: "get",
			middleware: [authorize("admin", "view")],
			route: "/",
			lib: require(libPath + "/getQuotes.js")()
		}	
	];
	
	for(var i = 0; i < routes.length; i++) {
		r = routes[i];
		logger.debug("Building " + r.method + " method for " + r.route);
		route[r.method](r.route, r.middleware, r.lib);
		logger.silly("Built " + r.method + " method for " + r.route);
	}	

	logger.debug("Quote Endpoints Created");

	return route;
	

};
