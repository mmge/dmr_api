module.exports = function(err) {
	
	var xml2js = require("xml2js");
	var response;
	
	if(err === undefined) {
		
		response = {
			status: this.locals.status,
			message: this.locals.message
		};
		
	} else {
		var e = JSON.parse(JSON.stringify(err));
		this.app.logger.debug(e);		
//		var response = e;
		response = {
			status: e.status,
			message: e.message,
			error: e
		};		
	}
	
	var format = this.req.params.format || 
				 this.req.body.format || 
				 this.req.query.format || 
				 this.req.accepts(['json', 'xml', 'html', 'text']) ||
				 this.app.locals.default_format ||
				 "text";
				
	this.app.logger.debug("format: " + format);
				
	switch(format) {
		case "html":
		case "json":
			this.app.logger.debug("Sending JSON response...");
			this.status(response.status).json(response);
			this.app.logger.debug("JSON response sent...");
			break;
		case "xml":
			this.app.logger.debug("Sending XML response...");
			
/*			if(Array.isArray(response.message)) {
				response.message.length; 
				var obj = {};
				for(var i = 0; i < response.message.length; i++) {
					obj[i] = response.message[i];
				}
				response.message = obj;
			} */
			var builder = new xml2js.Builder({rootName: "response"});
			var xml = builder.buildObject(JSON.parse(JSON.stringify(response)));
			return this.type("text/xml").status(response.status).send(xml);
		case "text":
			this.app.logger.debug("Sending text response...");
			return this.type("text/plain").status(response.status).send(response);
		default:
			this.app.logger.debug("Sending default plain text response...");
			return this.type("text/plain").status(400).send("Unsupported return format: " + format);
	}
	
};
