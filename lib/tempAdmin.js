module.exports = function(app) {

	app.database.connection.db.listCollections({name: "users"}).next(function(err, collinfo) {
		
		if(!collinfo) {
								
			var default_admin = new app.locals.User({
				username: "temp_admin",
				password: "password123",
				role: "admin"
			});

			default_admin.save(function(err) {
				if(err) {
					return app.logger.error("Unable to create default administrator in database.");
				}
				
				return app.logger.info("Created default api admin 'temp_admin' with password 'password123'. Please use this account to create a permanent admin, then delete it.");
							
			});
		
		} else {
			
			return app.logger.info("Detected User collection in database. Skipping default administrator creation.");
		
		}
		
	});

};
