module.exports = function() {
		
	var validateToken = require("./validateToken.js");
		
	return function(req, res, next) {
		
		var logger = req.app.logger;
				
		var options = {
			role: req.body.role || "viewer",
			actions: req.body.actions || ["view"]
		};
		
		if(req.body.exp) {
			options.exp = req.body.exp;
		}		
		
		validateToken(req.user, options, function(err, valid) {
			
			if(err) return next(err);
		
			if(valid) {
		
				req.app.locals.Token.create(req.user, options, function(err, token) {
				
					if(err) return next(err);
					
					res.status(201).type("text/plain").send(token);

					
				});
				
			} else {
				
				var Uerr = new Error();
				Uerr.status = 500;
				Uerr.message = "Could not validate token request.";
				return next(Uerr);
				
			}
		
		});
	};
	
};
