module.exports = function(app) {
	
	var logger = app.logger,
		db = app.database;
		
	var uuid = require("uuid"),
		jwt = require("jwt-simple"),
		juration = require("juration"),
		secret = require('../secret.js')();
		
	var roles = ["admin", "internal", "external", "viewer"];
	var actions = ["view", "create", "delete", "modify"];
	
	var scopeSchema = new db.Schema({
		role: {type: String, required: true, enum: roles, default: "viewer"},
		actions: [{type: String, required: true, enum: actions, default: "view"}]
	});
	
	var tokenSchema = new db.Schema({
		iss: {type: String, required: true, default: "IU_MMGE"},
		iat: {type: Date, required: true},
		aud: {type: String, required: true, default: "biosend"},
		jti: {type: String, required: true, index: true},
		sub: {type: String, required: true},
		exp: {type: Date},
		scope: scopeSchema,
		valid: {type: Boolean, required: true, default: true},
		issuedBy: {type: String, required: true}		
	});
	
	tokenSchema.statics.create = function(user, options, cb) {
		
		
		var claims = {
			iss: "IU_MMGE",
			iat: parseInt(Date.now()/1000, 10),
			aud: "biosend",
			sub: options.sub || user.username,
			jti: uuid.v1(),
			scope: {
				role: options.role,
				actions: options.actions
			}
		};
		
		if(options.exp) {
			claims.exp = claims.iat + juration.parse(options.exp);
		}
				
		var token = new Token({
			iss: claims.iss,
			iat: claims.iat,
			aud: claims.aud,
			sub: claims.sub,
			jti: claims.jti,
			scope: claims.scope,
			issuedBy: user.username		
		});
		
		if(claims.exp) {
			token.set({exp: claims.exp});
		}
		
		token.save(function(err) {
			if(err) return cb(err);
			return cb(null, jwt.encode(claims, secret));
		});
		
	};
	
	tokenSchema.statics.invalidate = function(user, token, cb) {
		
		var token_decoded = jwt.decode(token, secret);
				
		Token.findOne({jti: token_decoded.jti}, function(err, token_record) {
						
			if(err) return cb(err);
						
			if(!token_record) {
				var err = new Error();
				err.status = 404;
				err.message = "No matching token found.";
				return cb(err);
			}			
			
			// Users can only revoke their own tokens unless they are an admin.
			if(user.role !== "admin" && user.username !== token_record.issuedBy) {
				var err = new Error();
				err.status = 403;
				err.message = "You do not have permission to revoke this token.";
				return cb(err);
			}
			
			if(token_record.valid) {		
				token_record.set({valid: false});
					
				token_record.save(function(err) {
					if(err) return cb(err);
					cb(null, true);
				});
			} else {
				
				var err = new Error();
				err.status = 400;
				err.message = "Supplied token already revoked.";
				return cb(err);
				
			}
		});
		
	};
	
	tokenSchema.methods.invalidate = function(cb) {
		this.valid = false;
		this.save(function(err) {
			if(err) return cb(err);
			cb(null, true);
		});
	}; 
	
	var Token = db.model('Token', tokenSchema);
	
	return Token;
	
};
