module.exports = function(user, options, cb) {
	
	// This must be in order from highest priv to lowest priv
	var roles = ["admin", "internal", "external", "viewer"];
	
	var user_role = user.role;
	var token_role = options.role;
	
	var error = new Error();
	
	var valid = true;
	
	if(roles.indexOf(token_role) === -1) {
		error.status = 400;
		error.message = "You must provide a valid role for this token.";
		return cb(error, false);
	} else if(roles.indexOf(token_role) < roles.indexOf(user_role)) {
		error.status = 403;
		error.message = "You do not have permission to create a token of this type.";
		return cb(error, false);
	} else {
		cb(null, true);
	}
	
};
