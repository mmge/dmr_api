module.exports = function() {
	
	var jwt = require("jwt-simple");
	
	return function(req, res, next) {
		
		var logger = req.app.logger;
				
		req.app.locals.Token.invalidate(req.user, req.params.token, function(err, revoked) {
						
			if(err) return next(err);
			
			if(revoked) {
				res.locals.status = 200;
				res.locals.message = "Token revoked";
				res.respond();
			} else {
				var err = new Error();
				err.status = 500;
				err.message = "Unknown Internal Server Error";
				return next(err);
			}
			
		});
		
	};
	
};
