module.exports = function(console_level) {

	var level = console_level || "info";
	var fs = require("fs");
	var log_dir = "/dmr_api/logs/";
	
	fs.stat(log_dir, function(err, val) {
		if(err) {
			fs.mkdir(log_dir);
		}
	});

	var winston = require("winston");
	return new winston.Logger({
		transports: [
			new (winston.transports.File)({
				
				name: 'info-file',
				filename: log_dir + 'filelog-info.log',
				handleExceptions: true,
				json: true,
				maxsize: 5242880,
				maxFiles: 5,
				level: 'info'
				
			}),
			new winston.transports.Console({
				
				level: level,
				handleExceptions: true,
				colorize: true
				
			}),
			new (winston.transports.File)({
				
				name: 'error-file',
				filename: log_dir + 'filelog-error.log',
				handleExceptions: true,
				json: true,
				maxsize: 5242880,
				maxFiles: 5,
				level: 'error'
				
			})		
		],
		exitOnError: false	
	});
	
};
