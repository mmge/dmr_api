module.exports = function(options) {

	var express = require("express"),
		bodyParser = require("body-parser"),
		fs = require("fs");
			
	var exports = {};

	// Adds any required default options in case this modules was called
	// from somewhere other than dmr_api.js (Like from a testing script)
	var default_options = {
		port: 3000,
		db_loc: "localhost",
		db_name: "dmr",
		api_entry: "/api/vi",
		console_level: "debug",
		default_format: 'json',
		env: 'production',
    aud: 'biosend'
	};
	for(var key in Object.keys(default_options)) {
		var id = Object.keys(default_options)[key];
		options[id] = options[id] || default_options[id];
	}
	
	// Adds the custom response method that most of the endpoints use.
	express.response.respond = require("./response.js");
	
	// Instantiates an instance of an express server.
	var app = express();
	
	// Make the uploads directory if it doesn't exist already
	fs.stat("./uploads", function(err, val) {
		if(err) {
			fs.mkdir("./uploads");
		}
	});
	
	// Tells the express server if we are in production or development mode.
	app.set("env", options.env);
	app.locals.default_format = options.default_format;
 
  // Tells the express server what aud value to assign to generated tokens.
  app.locals.aud = options.aud;
	
	// Add the winston logger to the app
	app.logger = require("./logger.js")(options.console_level);
	app.logger.debug("Logger added to express app...");
		
	// add the mongoose instance to the app
	require("./dbConn.js")(app, options, function() {
		
				
		// Add a temporary administrator to the API, if neccessary.
		require("./tempAdmin.js")(app);
		
		app.logger.debug("Database connection established...");
		
	});
	
	// create a connection to static resources
	app.use("/static", express.static(__dirname + "/static"));
	app.logger.debug("Static resource directory established...");
	
	/*------------------------------------------------------------------ 
	This is where you would place any non-error handling middleware 
	that you want applied to all endpoints in the api.
	------------------------------------------------------------------*/
	
	// Adds the json body parsing middleware to all endpoints.
	app.use(function(req, res, next) {
		next();
	});
	
	app.use(bodyParser.json());
	
/*	app.use(function(req, res, next) {
		
		app.logger.debug("Received Request!");
		app.logger.debug("baseUrl: " + req.baseUrl);
		app.logger.debug("hostname: " + req.hostname);
		app.logger.debug("method: " + req.method);
		app.logger.debug("path: " + req.path);
		
		next();
		
	});
*/	
	/*------------------------------------------------------------------
	This is where you should load your routes
	------------------------------------------------------------------*/
	
	// This adds all the endponts related to orders.
	app.use(options.api_entry + "/orders", require("./orders.js")(app));
	app.logger.debug("Orders endpoints established...");
	
	// This adds all the endpoints related to the catalogs.
	app.use(options.api_entry + "/catalogs", require("./catalogs.js")(app));
	app.logger.debug("Catalogs endpoints established...");
	
	// This adds all the endpoints related to token generation
	app.use(options.api_entry + "/tokens", require("./tokens.js")(app));
	app.logger.debug("Token endpoints established...");
	
	// This adds all the endpoints related to user management
	app.use(options.api_entry + "/users", require("./users.js")(app));
	app.logger.debug("User endpoints established...");
	
	// This adds all the endpoints related to quote requests
	app.use(options.api_entry + "/quotes", require("./quotes.js")(app));
	app.logger.debug("Quote request endpoints established...");
	
	// This adds all the endpoints related to server testing
	app.use(options.api_entry + "/test", require("./test.js")(app));
	app.logger.debug("Server test endpoints established...");
	
	/*------------------------------------------------------------------
	This is where you should add any error handling middleware. 
	Error handling middleware should be the LAST middleware added to
	the app.
	------------------------------------------------------------------*/
	
	// This adds the error handling middleware
	app.use("*", require("./middleware/errors.js")(app));
	app.logger.debug("Adding error handling middleware...");
	
	/*------------------------------------------------------------------
	This is where you should capture any generic errors like 404 or 500.
	------------------------------------------------------------------*/
	
	// 400 - Bad Request
	logger.debug("Building 400 - Bad Request endpoint");
	app.use(function(req, res, next) {
		var err400 = new Error();
		err400.status = 400;
		err400.message = "Bad Request - No valid routes found.";
		res.respond(err400);		
	});
	logger.silly("Built 400 - Bad Request endpoint");
	
	// 500 Error
	logger.debug("Building 500 - Server Error endpoint.");
	app.use(function(req, res) {
		var err500 = new Error();
		err500.status = 500;
		err500.message = "Internal Server Error - An unknown internal server error has occured.";
		res.respond(err500);		
	});
	logger.silly("Built 500 - Server Error endpoint.");	
	
	app.logger.debug("Express Server Setup Complete...");
	
	// This creates the startServer() function that will start the actual
	// api server. It will be https if a key and cert is provided or
	// http if it they are not.	
	if(options.key !== undefined && options.cert !== undefined) {
		
		startServer = function() {
			var https = require("https");
			app.logger.debug("Attempting to create https server...");
			
			var server = https.createServer({
				key: fs.readFileSync(options.key),
				cert: fs.readFileSync(options.cert)
			}, app);
			
			server.listen(options.port, function() {
				app.logger.info("HTTPS server started. Listening on port " + 
					options.port + "...");
			});
			
			app.locals.server = server;
		};
		
	} else {
		
		startServer = function() {
			app.logger.debug("Attempting to create http server...");
			app.listen(options.port, function(err, value) {
				if(err) return app.logger.error(err);
				app.logger.info("HTTP server started. Listening on port " + 
					options.port + "...");
			});
			
			app.locals.server = app;
			
		};
		
	}
	
	app.locals.options = options;
	app.startServer = startServer;
	app.stopServer = function() {app.locals.server.close();};
	return app;
	
};
