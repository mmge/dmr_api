
module.exports = function(schema) {
	
	var Joi = require("joi");

	var Schema = {
		orders: Joi.object().keys({
			orderID: Joi.string().token(),
			format: Joi.string().regex(/^(json|html|xml|txt)$/),
			docID: Joi.string().token()
		})
	};
	
	return function(req, res, next) {
		req.app.logger.debug("Path Validation Middleware called...");
		Joi.validate(req.params, Schema.orders, function(err, value) {
			if(err) {
				err.status = 400;
				err.message = "Improperly formatted request."
				req.app.logger.error(err);
				return next(err);
			}	
			return next();	
		});
	};
	
};
