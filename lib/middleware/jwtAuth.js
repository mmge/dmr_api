module.exports = function(app) {

	var passport = require("passport");
	var jwtStrategy = require("passport-jwt").Strategy;
	var extractJwt = require("passport-jwt").ExtractJwt;
	
	var opts = {
		jwtFromRequest: extractJwt.fromAuthHeader(),
		secretOrKey: require("../secret.js")(),
		issuer: "IU_MMGE",
		audience: "biosend",
		ignoreExpiration: false
	};
	
	passport.use(new jwtStrategy(opts, function(jwt_payload, done) {
		
		app.locals.Token.findOne({jti: jwt_payload.jti}, function(err, token) {
			
			if(err) return done(err);
			
			if(!token) return(null, false);
			
			if(token.valid) {
				return done(null, jwt_payload);
			} else {
				return done(null, false);
			}
			
		});
				
	}));
	
	return function(req, res, next) {
		
		req.app.logger.debug("Authenticating Token...")
		
		passport.authenticate('jwt', {session: false}, function(err, jwt, info) {
			
			req.app.logger.debug("Entered passport.authenticate...");
			
			if(err) {
				app.req.logger.error(err);
				return next(err);
			}
			
			if(!jwt) {
				var err = new Error();
				err.status = 401;
				err.message = "The provided jwt could not be verified.";
				req.app.logger.error(err)
				return next(err);
			}
			
			req.user = jwt;
			
			req.app.logger.debug("JWT Authenticated...");
			return next();
			
			
		})(req, res, next);
		
	};
	
//	return passport.authenticate('jwt', {session: false});

};
