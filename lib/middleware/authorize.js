module.exports = function(levels) {

	return function(req, res, next) {
	
		if(levels.indexOf(req.user.role) === -1) {
		
			var err = new Error();
			err.status = 403;
			err.message = "You do not have authorization to access this endpoint.";
			return next(err);
		
		}	
		
		next();
	
	};

};
