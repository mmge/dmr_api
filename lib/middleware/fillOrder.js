module.exports = function() {

	return function(req, res, next) {

		req.app.logger.debug("Filling order " + req.params.orderID + " from database.");
		res.locals.order = new res.app.locals.Order();
		res.locals.order.getById(req.params.orderID, next);
		req.app.logger.debug("Order Filled");
	
	};

};
