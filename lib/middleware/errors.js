module.exports = function(app) {

	if(app.get("env") === "development") {
		
		return function(err, req, res, next) {
			
			req.app.logger.debug("Processing Error...");
			err.status = err.status || 500;
			err.message = err.message || "Unknown Error";
		
			app.logger.debug("Handling a development error: " + err);
			res.respond({
				status: err.status,
				message: err.message,
				error: err});
		
		};
		
	} else {

		return function(err, req, res, next) {

			req.app.logger.debug("Processing Error...");
			err.status = err.status || 500;
			err.message = err.message || "Unknown Error";
						
			app.logger.debug("Handling a production error: " + err);
			res.respond({
				status: err.status,
				message: err.message,
				error: {}});
							
		}

	}

}
