module.exports = function(roles, action) {

	if(roles === "all_roles") roles = ["admin", "internal", "external", "viewer"];

	return function(req, res, next) {
	
		req.app.logger.debug("Authorizing Token...");
		var token_actions = req.user.scope.actions;
	
		if(!Array.isArray(roles)) roles = [roles];
		if(!Array.isArray(token_actions)) token_actions = [token_actions];

		if(roles.indexOf(req.user.scope.role) > -1) {
			req.app.logger.debug("Role Authorized...");
			if(token_actions.indexOf(action) > -1) {
				
				req.app.logger.debug("Token Authorized to access endpoint...");
				return next();				
				
			} else {
				
				var aError = new Error();
				aError.status = 403;
				aError.message = "The supplied token cannot perform this action.";
				console.log("Pre Logger");
				req.app.logger.error(aError);
				console.log("Post Logger");
				return next(aError);
				// return next({status: 401, message: "I don't get it"});
			}
			
		} else {
			var roleError = new Error();
			roleError.status = 403;
			roleError.message = "The supplied token cannot access this endpoint.";
			req.app.logger.error(roleError);
			return next(roleError);
		}
	
	};

};
