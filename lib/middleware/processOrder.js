module.exports = function() {

	return function(req, res, next) {
	
		var logger = req.app.logger;
		
		if(res.locals.order) {
			res.locals.new_order = new res.app.locals.Order(res.locals.order);
		}
			
		next();
	
	}

};
