// Read the order file from a POST to /orders
module.exports = function() {

	var fs = require("fs"),
		xml2js = require("xml2js"),
		is = require("type-is");

	return function(req, res, next) {
	
		var logger = req.app.logger;
		
		logger.debug(req.files);
		
		if(req.files.order) {
			var mimetype = req.files.order[0].mimetype;
			var order_file = __dirname + "/../../uploads/" + req.files.order[0].filename;
			
			if(is.is(mimetype, ["*/xml"])) {
				logger.debug("XML order file detected...");
				logger.debug("XML File: " + req.files.order[0].filename);
//				var order_file = req.files.order[0].buffer.toString('utf8');
				var parser = new xml2js.Parser({trim: true, explicitRoot: false, explicitArray: false});
				fs.readFile(order_file, function(err, xml_string) {
					if(err) {
						logger.error(err);
						err.status = 500;
						err.message = "Uploaded XML order file could not be read.";
						return(err);
					}
					parser.parseString(xml_string, function(err, xml_data) {
						
						if(err) {
							logger.error(err);
							err.status = 500;
							err.message = "Error processing uploaded XML order file.";
							return next(err);
						}
						
						res.locals.order = new res.app.locals.Order();
						res.locals.order.getFromDMRXML(xml_data, function(err) {
							
							if(err) {
								logger.error(err);
								return next(err);
							}
							
							logger.debug("It looks like it read the XML file...");
							return next();
							
						});
						
					});			
			})
			} else if(is.is(mimetype, ["*/json"])) {
				logger.debug("JSON order file detected...");
				fs.readFile(order_file, function(err, json_string) {
					if(err) return(next(err));
					res.locals.order = new res.app.locals.Order();
					res.locals.order.getFromJSON(JSON.parse(json_string), function(err) {
						
						if(err) {
							logger.error(err);
							return next(err);
						}
						
						logger.debug("It looks like it read the JSON file...");
						return next();
						
					});
				});
			} else {
				logger.debug("Unsupported order file detected...");
				var err = new Error();
				err.status = 400;
				err.message = "Uploaded order file not recognized as an acceptable format. Please upload either XML or JSON files.";
				return next(err);		
			}
	
		} else {
			
			if(req.body.order) {
				res.locals.order = new res.app.locals.Order();
				res.locals.order.getFromBody(req.body.order, function(err) {
					if(err) {
						logger.error(err);
						return next(err);
					}
					
					logger.debug("It looks like it read the body content...");
					return next();
					
				});
			} else {
			
				logger.debug("No order information uploaded...");
				next();
				
			}
			
		} 
	
	};

};
