module.exports = function(app) {

	var passport = require("passport");
	var basicStrategy = require("passport-http").BasicStrategy;
	var User = app.locals.User;
	
	passport.use(new basicStrategy(
		function(username, password, callback) {
			User.findOne({username: username}, function(err, user) {
				if(err) return callback(err);
				
				if(!user) return callback(null, false);
				
				user.login(password, function(err, isMatch) {
					if(err) return callback(err);
					
					if(!isMatch) return callback(null, false);
					
					return callback(null, user);
				
				});			
			
			});		
		}
	));
	
	return function(req, res, next) {
		
		passport.authenticate('basic', {session: false}, function(err, user, info) {
			
			if(err) {
				return next(err);
			}
			
			if(!user) {
				var err = new Error();
				err.status = 401;
				err.message = "Incorrect username/password";
				return next(err);
			}
			
			req.user = user;
			
			return next();
			
		})(req, res, next);
	
	};
	
//	return passport.authenticate('basic', {session: false});

};
