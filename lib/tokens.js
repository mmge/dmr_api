module.exports = function(app) {

	var logger = app.logger;

	var express = require("express");
	var passport = require('passport');
	var BasicStrategy = require('passport-http').BasicStrategy;	
	
	var route = express.Router();
	
	var basicAuth = require("./middleware/basicAuth.js")(app);
	var authorize = require("./middleware/authorize.js");

	// ADD MIDDLEWARE HERE //
	route.use(basicAuth);
			
	var libPath = "./tokens";

	var routes = [
		{
			method: "post",
			middleware: [authorize(['admin', 'external'])],
			route: "/",
			lib: require(libPath + "/createToken.js")()
		},
		{
			method: "delete",
			middleware: [authorize(['admin', 'external'])],
			route: "/:token",
			lib: require(libPath + "/revokeToken.js")()
		}
	];
	
	for(var i = 0; i < routes.length; i++) {
		r = routes[i];
		logger.debug("Building " + r.method + " method for " + r.route);
		route[r.method](r.route, r.middleware, r.lib);
		logger.silly("Built " + r.method + " method for " + r.route);
	}	
	
	logger.debug("User Endpoints Created");

	return route;
		
};
