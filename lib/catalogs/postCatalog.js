

module.exports = function() {
	
	var fs = require("fs"),
		is = require("type-is"),
		csv = require("fast-csv");	
		
	return function(req, res, next) {
		
		var logger = req.app.logger;
		
		req.app.locals.Catalog.clearCatalog(function(err, num) {
			
			if(err) return next(err);
		
			var catalog_file = __dirname + "/../../uploads/" + req.files.catalog[0].filename;
								
			var csv_data = fs.createReadStream(catalog_file);
				
			logger.debug("readstream Created");
				
			var csvStream = csv()
				.on("data", function(data) {
					req.app.locals.Catalog.create({
						SampleId: data[0],
						RepositoryName: data[1],
						SampCollTyp: data[2],
						VisitTypPDBP: data[3],
						GUID: data[4],
						SampleUnitNum: data[5],
						SampleUnitMeasurement: data[6],
						Inventory_BiorepositoryCount: data[7],
						ADDTL_STOCK: data[8],
						ConcentrationDNA_RNA: data[9],
						ConcentrationDNA_RNAUnits: data[10],
						Sample260_280Ratio: data[11],
						Sample260_230Ratio: data[12],
						RNAIntegrityNum: data[13],
						rRatio: data[14],
						RNAQualityScale: data[15],
						SampleClottingInd: data[16],
						SampleTurbidityScale: data[17],
						SampleAvgHemoglobinVal: data[18],
						SampleAvgHemoglobinUnits: data[19],
						SampleAvgHemoglobinResult: data[20],
						SampleAvgHemoglobinResultDesc: data[21],
						SampleHemolysisScale: data[22],
						Inventory_BiorepositoryDate: data[23],
						Study: data[24]
					}, function(err, record) {
						if(err) return next(err);
					});
				})
				.on("end", function() {
					logger.info("New Catalog uploaded");
					res.locals.status = 201;
					res.locals.message = "New Catalog uploaded";
					return res.respond();
				});
		
			csv_data.pipe(csvStream);
	
		});	
		
	};	
	
};
