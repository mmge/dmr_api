module.exports = function() {
	
	var fs = require("fs"),
		csv = require("fast-csv");
	
	return function(req, res, next) {

		var query = req.query;

		var keys = Object.keys(query);

		for(var i = 0; i < keys.length; i++) {
			query[keys[i]] = query[keys[i]].split(",");
		}

		var q = {};
		
		if(keys.indexOf("SampCollTyp") !== -1) {
			q.SampCollTyp = {$in: query.SampCollTyp};
		}
		
		if(keys.indexOf("VisitTypPDBP") !== -1) {
			q.VisitTypPDBP = {$in: query.VisitTypPDBP};
		}
		
		if(req.params.catalog !== "catalog") {
			q.Study = req.params.catalog;
		}
		
		var colNames = 
			["SampleId",
			"RepositoryName",
			"SampCollTyp",
			"VisitTypPDBP",
			"GUID",
			"SampleUnitNum",
			"SampleUnitMeasurement",
			"Inventory_BiorepositoryCount",
			"ADDTL_STOCK",
			"ConcentrationDNA_RNA",
			"ConcentrationDNA_RNAUnits",
			"Sample260_280Ratio",
			"Sample260_230Ratio",
			"RNAIntegrityNum",
			"rRatio",
			"RNAQualityScale",
			"SampleClottingInd",
			"SampleTurbidityScale",
			"SampleAvgHemoglobinVal",
			"SampleAvgHemoglobinUnits",
			"SampleAvgHemoglobinResult",
			"SampleAvgHemoglobinResultDesc",
			"SampleHemolysisScale",
			"Inventory_BiorepositoryDate"];
				
		logger.debug(q);
				
		var catalog = req.app.locals.Catalog.find(q,
			function(err, records) {
				
				if(err) return next(err);
				
//				records.unshift(columns);
				
				csv.writeToString(records,
					{
						headers: true,
						transform: 
							function(row) {
								var op = {};
								colNames.forEach(function(el, index, array) {
									op[el] = row[el];
								});

								return op;
							}
						
					},
					function(err, data) {
						if(err) return next(err);
						
            fs.writeFileSync("/var/catalogs/catalog" + Date.now() + ".csv", data);
                           
						res.type("text/csv").send(data);
						
					});
				
				
			});
		
		
	};	
	
};
