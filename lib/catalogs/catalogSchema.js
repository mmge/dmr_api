module.exports = function(app) {

	var sampleTypes = ["Plasma", "RNA", "Serum", "Whole Blood", "DNA", 
					   "Urine", "Cerebrospinal Fluid", "Saliva"];

	var fs = require("fs");

	var logger = app.logger,
		db = app.database,
		grid = app.grid;

	var catalogSchema = new db.Schema({
		SampleId: {type: String, required: true, index: true},
		RepositoryName: {type: String, required: true},
		SampCollTyp: {type: String, required: true},
		VisitTypPDBP: {type: String, required: true},
		GUID: {type: String, required: true},
		SampleUnitNum: {type: String, required: false},
		SampleUnitMeasurement: {type: String, required: false},
		Inventory_BiorepositoryCount: {type: String, required: true},
		ADDTL_STOCK: {type: String, required: true},
		ConcentrationDNA_RNA: {type: String, required: false},
		ConcentrationDNA_RNAUnits: {type: String, required: false},
		Sample260_280Ratio: {type: String, required: false},
		Sample260_230Ratio: {type: String, required: false},
		RNAIntegrityNum: {type: String, required: false},
		rRatio: {type: String, required: false},
		RNAQualityScale: {type: String, required: false},
		SampleClottingInd: {type: String, required: false},
		SampleTurbidityScale: {type: String, required: false},
		SampleAvgHemoglobinVal: {type: String, required: false},
		SampleAvgHemoglobinUnits: {type: String, required: false},
		SampleAvgHemoglobinResult: {type: String, required: false},
		SampleAvgHemoglobinResultDesc: {type: String, required: false},
		SampleHemolysisScale: {type: String, required: false},
		Inventory_BiorepositoryDate: {type: String, required: false},
		Study: {type: String, required: true}
	});
	
	catalogSchema.statics.clearCatalog = function(cb) {
		
		Catalog.remove({}, cb);
		
	};
	
	catalogSchema.statics.addRecord = function(record, cb) {
		
		Catalog(record).save(cb);
		
	}
	
	Catalog = db.model('Catalog', catalogSchema);
			
	return Catalog;
	
};
				
	
